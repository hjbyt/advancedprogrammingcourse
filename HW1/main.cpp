#include <iostream>

#include "Piece.h"
#include "Puzzle.h"
#include "PuzzleValidatorBase.h"
#include "InputParser.h"

////////////////////////////////////////////

int main(int argc, char* argv[]) {
    InputParser parser(argc, argv);
    if (PrintUsage == parser.valid_parameters) {
        std::cout << "Usage: ex1 <input file> <output file> [-rotate]" << std::endl;
        return -1;
    } else if (InvalidInput == parser.valid_parameters) {
        return -1;
    }

    try {
        std::vector<Piece> pieces;
        if (!parser.parsePieces(pieces)) {
            return -1;
        }
        
        Puzzle puzzle(pieces);
        auto validator = parser.getValidator(puzzle);
        if (!validator->validateCanSolve(parser.getOutput())) {
            return -1;
        }

        if (puzzle.solve(parser.rotations_allowed, parser.thread_count)) {
            puzzle.print(parser.getOutput());
            return 0;
        } else {
            parser.getOutput() << "Cannot solve puzzle: it seems that there is no proper solution" << std::endl;
            return -1;
        }
    } catch (std::exception& e) {
        parser.getOutput() << "Unexpected Error: " << e.what() << std::endl;
        return -1;
    }
}