#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>
#include <vector>

#include "Piece.h"
#include "Puzzle.h"
#include "PuzzleValidatorBase.h"

enum InputValidationStatus {
    ValidInput,
    PrintUsage,
    InvalidInput
};

////////////////////////////////////////////
class InputParser {
public:
    InputParser(int argc, char* argv[]);
    bool parsePieces(std::vector<Piece>& pieces);
    std::ostream& getOutput();
    std::istream& getInput();
    std::unique_ptr<PuzzleValidatorBase> getValidator(Puzzle& puzzle);
    
    InputValidationStatus valid_parameters;
    bool rotations_allowed;
    unsigned int thread_count;
    std::string input_file;
    std::string output_file;

private:
    std::ofstream output_file_stream;
    std::ifstream input_file_stream;
    bool use_cout;
    bool use_cin;

    void parseFirstLine(int& pieces_count);
    std::vector<Piece> tryParsePieceLines(int piece_count, std::vector<std::string>& parse_exceptions);
    bool checkForMissingElements(const std::vector<Piece>& pieces, int expected_count);
    bool checkForWrongElements(const std::vector<Piece>& pieces, int expected_count);
    bool printParseExceptions(const std::vector<std::string>& parse_exceptions);
    std::string prepareForPrinting(std::vector<int>& id_list);
};