#pragma once

#include "PuzzleValidatorBase.h"

#include <vector>

class RotationPuzzleValidator : public PuzzleValidatorBase {
public:
	RotationPuzzleValidator(Puzzle& puzzle);
	virtual ~RotationPuzzleValidator() {}

protected:
	virtual bool checkStraightEdgesAndSetPossibleDimensions();
	virtual bool hasCornerPiece(CornerType cornerType);
	virtual bool isEdgesSumZero();

private:
	bool isCorner(const Piece& piece);
	std::vector<int> used_corners;
};