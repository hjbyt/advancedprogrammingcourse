#pragma once

#include "LookupBase.h"

class RotationLookup : public LookupBase {
public:
	RotationLookup(int width, int height, const std::vector<Piece>& pieces);
	virtual ~RotationLookup() { }

protected:
	virtual void addPieceLookups(const Piece& piece) override;
	virtual void removePieceLookups(const Piece& piece) override;
	virtual Piece fromPossiblePieces(LookupEdgeType top, LookupEdgeType bottom, LookupEdgeType left, LookupEdgeType right) override;

private:
	struct IdAndRotation {
		int id;
		Rotation rotation;

		IdAndRotation(const int id, const Rotation rotation);
		bool operator==(const IdAndRotation& other);
	};
	std::vector<IdAndRotation> piece_ids[NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS];

	void rotatePiecesLeft(LookupEdgeType& left, LookupEdgeType& top, LookupEdgeType& right, LookupEdgeType& bottom);
};