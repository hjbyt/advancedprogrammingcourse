#pragma once

#include "Piece.h"
#include "Position.h"
#include "Vector2d.h"

#include <assert.h>

// This is just defined to be EdgeType+1 so we can use it an array
enum class LookupEdgeType {
	Female = 0,
	Straight = 1,
	Male = 2,
	Any = 3,
};

inline LookupEdgeType toLookup(EdgeType type) {
	return static_cast<LookupEdgeType>(static_cast<int>(type) + 1);
}
inline EdgeType toEdgeType(LookupEdgeType type) {
	return static_cast<EdgeType>(static_cast<int>(type) - 1);
}

static const int NUMBER_OF_EDGES_LOCATIONS = 4;

struct Location {
	int x;
	int y;

	Location(int x, int y);
};

////////////////////////////////////////////
// This class holds the actuall puzzle solving logic (and is a base for
// either the DirectLookup which solves puzzles without rotations or the
// RotationLookup which solves puzzles with rotations).
// The way we solve the puzzle is by looking at the constraints each position
// in the puzzle has on it. For example, at the start the only constraints are
// each edge at the external ring of the puzzle should be straight, so we add all
// those constraints to the relevant positions. We use a datastructure like described
// in the description of part 2 in order to speed up finding how many pieces are available
// for each position (and what pieces as well). We improve a bit over the described structure
// by also counting how many unique pieces we have for each location (where unique pieces mean
// we won't try 2 pieces if they are identical) and each time using the place with the least
// possible unique pieces (with the current constraints on the position). 
// The only difference between the version with rotations and the version without rotations
// is the number of places we update when we insert a piece into the lookup and what we insert
// (because for the non-rotation we need to save only the piece's ID, but for the rotation we
// also need to save what rotation it needs to be in to fit in the current position).
////////////////////////////////////////////
class LookupBase {
public:
	LookupBase(int width, int height);
	virtual ~LookupBase() { }
	Location getNextLocation();
	std::vector<Piece> getOptionsForLocation(const Location& location);
	void setPosition(const Location& position, const Piece& piece);
	void clear(const Location& position);
	Piece getAt(const Location& location) const;
	int width() const;
	int height() const;

protected:
	virtual void addPieceLookups(const Piece& piece) = 0;
	virtual void removePieceLookups(const Piece& piece) = 0;
	virtual Piece fromPossiblePieces(LookupEdgeType top, LookupEdgeType bottom, LookupEdgeType left, LookupEdgeType right) = 0;
	
	int getOptionCountForPosition(Location& location);
	void loopPieceLocations(const Piece& piece, int modifier, int table[NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS]);
	void addConstraint(Location location, EdgeLocation edge_location, EdgeType lookupType);
	void removeConstraint(Location location, EdgeLocation edge_location);
	bool isUsed(Location& location);

	Vector2d<Position> positions;
	int possible_pieces[NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS];
	int unique_pieces[NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS];

	struct Options {
		Options(LookupEdgeType lookup);

		typedef	LookupEdgeType* iterator;
		typedef	const LookupEdgeType* const_iterator;

		iterator begin();
		const_iterator begin() const;
		iterator end();
		const_iterator end() const;

	private:
		LookupEdgeType _lookup;
	};
};