#pragma once

#include <stdexcept>
#include <iostream>
#include <string>
#include <limits>
#include <initializer_list>
#include <array>
#include <algorithm>
#include <iterator>

template<int D, int K>
struct PuzzlePiece {
    enum {
        SIDES = D * 2
    };

    PuzzlePiece(std::initializer_list<int> sides_) {
        if (sides_.size() != SIDES) {
            throw new std::range_error("Incorrect number of sides");
        }
        std::move(sides_.begin(), sides_.end(), std::begin(this->sides));

        for (auto side : sides) {
            if (((side > K) || (side < -K)) && side != std::numeric_limits<int>::min()) {
                std::string message = "Can't initialize puzzle with max value " + std::to_string(K) + " with value " +
                                      std::to_string(side);
                throw new std::range_error(message);
            }
        }
    }

    std::array<int, SIDES> sides;

    enum {
        SIZE_RANGE = K
    };
    enum {
        DIMENSTIONS = D
    };
};

template<int D, int K>
std::ostream& operator<<(std::ostream& os, const PuzzlePiece<D, K>& piece) {
    auto iter = piece.sides.begin();
    auto end = piece.sides.end();
    auto end_prev = end - 1;
    for (; iter != end_prev; iter++) {
        os << *iter << ", ";
    }
    os << *end_prev;
    return os;

}

template<int K>
using Puzzle2dPiece = PuzzlePiece<2, K>;

template<int K>
using Puzzle3dPiece = PuzzlePiece<3, K>;
