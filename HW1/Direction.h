#pragma once

#include <array>

enum Direction {
    Left = 0,
    Top,
    Right,
    Bottom,
};
//Note: this is needed because gcc complains that EdgeType is not hashable
using DirectionInt = int;
static const int NUMBER_OF_DIRECTIONS = 4;
static const std::array<Direction, 4> DIRECTIONS = {Left, Top, Right, Bottom};

enum Rotation {
	Base = 0,
	One = 1,
	Two = 2,
	Three = 3
};

inline Direction realDirection(Direction desired, Rotation rotation) {
	return static_cast<Direction>((static_cast<int>(desired) - static_cast<int>(rotation) + NUMBER_OF_DIRECTIONS) % NUMBER_OF_DIRECTIONS);
}
static const std::array<Rotation, 4> ROTATIONS = { Base, One, Two, Three };