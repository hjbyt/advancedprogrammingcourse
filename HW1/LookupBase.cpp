#include "LookupBase.h"

#include <climits>
#include <cstring>

Location::Location(int _x, int _y) : x(_x), y(_y) {}

LookupBase::LookupBase(int width, int height) :
	positions(width, height)
{
	for (int x = 0; x < width; x++) {
		addConstraint(Location(x, 0), EdgeLocation::Top, EdgeType::Straight);
		addConstraint(Location(x, height - 1), EdgeLocation::Bottom, EdgeType::Straight);
	}
	for (int y = 0; y < height; y++) {
		addConstraint(Location(0, y), EdgeLocation::Left, EdgeType::Straight);
		addConstraint(Location(width - 1, y), EdgeLocation::Right, EdgeType::Straight);
	}
    std::memset(unique_pieces, 0, sizeof(unique_pieces));
	std::memset(possible_pieces, 0, sizeof(possible_pieces));
}

Piece LookupBase::getAt(const Location& location) const {
	return positions.get(location.x, location.y).piece;
}

int LookupBase::width() const {
	return positions.width;
}
int LookupBase::height() const {
	return positions.height;
}

Location LookupBase::getNextLocation() {
	int min_possiblities = INT_MAX;
	Location min_location = { -1, -1 };
	for (int y = 0; y < positions.height; y++) {
		for (int x = 0; x < positions.width; x++) {
			auto current_location = Location(x, y);
			if (isUsed(current_location)) {
				continue;
			}
			int current_posibilities = this->getOptionCountForPosition(current_location);
			if (current_posibilities < min_possiblities) {
				min_possiblities = current_posibilities;
				min_location = current_location;
			}
		}
	}
	return min_location;
}

void LookupBase::setPosition(const Location& location, const Piece& piece) {
	positions.access(location.x, location.y).setPiece(piece);
	this->removePieceLookups(piece);
	if (location.x > 0) {
		auto left = Location(location.x - 1, location.y);
		if (!isUsed(left)) {
			addConstraint(left, EdgeLocation::Right, getConstraint(piece.left()));
		}
	}
	if (location.x < positions.width - 1) {
		auto right = Location(location.x + 1, location.y);
		if (!isUsed(right)) {
			addConstraint(right, EdgeLocation::Left, getConstraint(piece.right()));
		}
	}
	if (location.y > 0) {
		auto above = Location(location.x, location.y - 1);
		if (!isUsed(above)) {
			addConstraint(above, EdgeLocation::Bottom, getConstraint(piece.top()));
		}
	}
	if (location.y < positions.height - 1) {
		auto below = Location(location.x, location.y + 1);
		if (!isUsed(below)) {
			addConstraint(below, EdgeLocation::Top, getConstraint(piece.bottom()));
		}
	}
}

void LookupBase::clear(const Location& location) {
	this->addPieceLookups(positions.get(location.x, location.y).piece);
	positions.access(location.x, location.y).clearPiece();
	if (location.x > 0) {
		auto left = Location(location.x - 1, location.y);
		if (!isUsed(left)) {
			removeConstraint(left, EdgeLocation::Right);
		}
	}
	if (location.x < positions.width - 1) {
		auto right = Location(location.x + 1, location.y);
		if (!isUsed(right)) {
			removeConstraint(right, EdgeLocation::Left);
		}
	}
	if (location.y > 0) {
		auto above = Location(location.x, location.y - 1);
		if (!isUsed(above)) {
			removeConstraint(above, EdgeLocation::Bottom);
		}
	}
	if (location.y < positions.height - 1) {
		auto below = Location(location.x, location.y + 1);
		if (!isUsed(below)) {
			removeConstraint(below, EdgeLocation::Top);
		}
	}
}

void LookupBase::loopPieceLocations(const Piece& piece, int modifier,
	int table[NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS]) {

	for (auto top : { piece.top(), EdgeType::Invalid }) {
		auto t = toLookup(top);
		for (auto bottom : { piece.bottom(), EdgeType::Invalid }) {
			auto b = toLookup(bottom);
			for (auto left : { piece.left(), EdgeType::Invalid }) {
				auto l = toLookup(left);
				for (auto right : { piece.right(), EdgeType::Invalid }) {
					auto r = toLookup(right);
					table[(int)t][(int)b][(int)l][(int)r] += modifier;
				}
			}
		}
	}
}

void LookupBase::addConstraint(Location location, EdgeLocation edge_location, EdgeType lookupType) {
	auto& position = positions.access(location.x, location.y);
	position.addRequirements(edge_location, lookupType);
}

void LookupBase::removeConstraint(Location location, EdgeLocation edge_location) {
	auto& position = positions.access(location.x, location.y);
	position.clearRequirements(edge_location);
}

bool LookupBase::isUsed(Location& location) {
	auto& position = positions.access(location.x, location.y);
	return position.used();
}

int LookupBase::getOptionCountForPosition(Location& location) {
	auto& position = positions.access(location.x, location.y);
	auto t = toLookup(position.getRequirements(EdgeLocation::Top));
	auto b = toLookup(position.getRequirements(EdgeLocation::Bottom));
	auto l = toLookup(position.getRequirements(EdgeLocation::Left));
	auto r = toLookup(position.getRequirements(EdgeLocation::Right));
	return unique_pieces[(int)t][(int)b][(int)l][(int)r];
}

std::vector<Piece> LookupBase::getOptionsForLocation(const Location& location) {
	std::vector<Piece> results;
	auto& position = positions.access(location.x, location.y);

	for (auto t : Options(toLookup(position.getRequirements(EdgeLocation::Top)))) {
		for (auto b : Options(toLookup(position.getRequirements(EdgeLocation::Bottom)))) {
			for (auto l : Options(toLookup(position.getRequirements(EdgeLocation::Left)))) {
				for (auto r : Options(toLookup(position.getRequirements(EdgeLocation::Right)))) {
					if (0 != possible_pieces[(int)t][(int)b][(int)l][(int)r]) {
						results.push_back(fromPossiblePieces(t, b, l, r));
					}
				}
			}
		}
	}
	return results;
}

static LookupEdgeType ALL_OPTIONS[3] = { LookupEdgeType::Female, LookupEdgeType::Male, LookupEdgeType::Straight };

LookupBase::Options::Options(LookupEdgeType lookup) : _lookup(lookup) {}

LookupBase::Options::iterator LookupBase::Options::begin() {
	if (LookupEdgeType::Any == _lookup) {
		return &(ALL_OPTIONS[0]);
	}
	return &_lookup;
}

LookupBase::Options::iterator LookupBase::Options::end() {
	if (LookupEdgeType::Any == _lookup) {
		return &(ALL_OPTIONS[0]) + 3;
	}
	return (&_lookup)+1;
}

LookupBase::Options::const_iterator LookupBase::Options::begin() const {
	if (LookupEdgeType::Any == _lookup) {
		return &(ALL_OPTIONS[0]);
	}
	return &_lookup;
}

LookupBase::Options::const_iterator LookupBase::Options::end() const {
	if (LookupEdgeType::Any == _lookup) {
		return &(ALL_OPTIONS[0]) + 3;
	}
	return (&_lookup) + 1;
}