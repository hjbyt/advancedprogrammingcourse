#pragma once

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <memory>

#include "Dimensions.h"
#include "Piece.h"
#include "Vector2d.h"
#include "LookupBase.h"


////////////////////////////////////////////
// This class represents the puzzle and finds the solution.
// The class itself is more of a manager, all the actuall searching logic
// is in the Lookup classes (although this class does validate the result
// in debug builds).
////////////////////////////////////////////
class Puzzle {
public:
    const std::vector<Piece>& pieces;

    ////////////////////////////////////////////
    explicit Puzzle(std::vector<Piece>& pieces);
    bool solve(bool rotations_allowed, unsigned int thread_count);
	void print(std::ostream& output) const;

	std::vector<Dimensions> getPossibleDimensions();
	void setPossibleDimensions(std::vector<Dimensions>& new_possible_dimensions);

private:
    using CornerMappings = std::unordered_map<int, std::vector<int>>;

    /////////////////////////////////////
	void doSolve(bool rotations_allowed, unsigned int thread_count);
    bool isSolutionValid();

    /////////////////////////////////////
    std::vector<Dimensions> possible_dimensions;
	std::unique_ptr<LookupBase> solution_lookup;
};