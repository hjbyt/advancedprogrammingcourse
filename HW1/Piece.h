#pragma once

#include <string>
#include <array>
#include "Direction.h"

//Note: we refrain from using enum class because it disallows implicit conversion to int, which is required in many parts of the code.
enum EdgeType {
    Straight = 0,
    Male = 1,
    Female = -1,
    Invalid = 2,
};
static const std::array<EdgeType, 3> VALID_EDGE_TYPES = {EdgeType::Female, EdgeType::Straight, EdgeType::Male};

inline int edgeTypeToIndex(EdgeType edge_type) {
    return (int)edge_type + 1;
}
inline EdgeType getConstraint(EdgeType my_type) {
	return static_cast<EdgeType>(0 - static_cast<int>(my_type));
}

typedef EdgeType Edges[DIRECTIONS.size()];

class Piece {
public:
	Piece();
    Piece(int id, const EdgeType (& edges)[DIRECTIONS.size()], Rotation rotation);
	Piece(const Piece& other);
	Piece(const Piece& other, Rotation rotation);
    static Piece fromLine(const std::string& line);
    const int& id() const;
    EdgeType top() const;
    EdgeType bottom() const;
    EdgeType left() const;
    EdgeType right() const;
    EdgeType getEdge(Direction location) const;
	Rotation getRotation() const;
    size_t hash() const;
	void print(std::ostream& output) const;

private:
    int _id;
    Edges _edges;
	Rotation _rotation;
};
