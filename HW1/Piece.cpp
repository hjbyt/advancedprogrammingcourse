#include "Piece.h"
#include <iostream>
#include <sstream>

void throwParseException(int id, const std::string& line);

////////////////////////////////////////////
Piece::Piece() : _id(-1), _rotation(Base) {
	for (unsigned int edge_location = 0; edge_location < DIRECTIONS.size(); ++edge_location) {
		this->_edges[edge_location] = EdgeType::Invalid;
	}
}

Piece::Piece(int id, const EdgeType (& edges)[DIRECTIONS.size()], Rotation rotation)
        : _id(id), _rotation(rotation) {
    for (unsigned int edge_location = 0; edge_location < DIRECTIONS.size(); ++edge_location) {
        this->_edges[edge_location] = edges[edge_location];
    }
}

Piece::Piece(const Piece& other) :
	Piece(other, other._rotation)
{
}

Piece::Piece(const Piece& other, Rotation rotation) :
	Piece(other._id, other._edges, rotation)
{
}

Piece Piece::fromLine(const std::string& line) {
    std::istringstream stream(line);
    int id;
    if (!(stream >> id)) {
        throwParseException(id, line);
    }
    EdgeType edges[DIRECTIONS.size()];
    for (unsigned int edge_location = 0; edge_location < DIRECTIONS.size(); ++edge_location) {
        int edge_int_value = 0;
        if (!(stream >> edge_int_value)) {
            throwParseException(id, line);
        }
        if (-1 <= edge_int_value && edge_int_value <= 1) {
            edges[edge_location] = (EdgeType)edge_int_value;
        } else {
            throwParseException(id, line);
        }
    }
    std::string s;
    if (stream >> s) {
        throwParseException(id, line);
    }
    return Piece(id, edges, Rotation::Base);
}

const int& Piece::id() const {
    return _id;
}

EdgeType Piece::top() const {
	return _edges[realDirection(Direction::Top, _rotation)];
}

EdgeType Piece::bottom() const {
	return _edges[realDirection(Direction::Bottom, _rotation)];
}

EdgeType Piece::left() const {
	return _edges[realDirection(Direction::Left, _rotation)];
}

EdgeType Piece::right() const {
	return _edges[realDirection(Direction::Right, _rotation)];
}

EdgeType Piece::getEdge(Direction location) const {
    switch (location) {
        case Direction::Left:
            return left();
        case Direction::Top:
            return top();
        case Direction::Right:
            return right();
        case Direction::Bottom:
            return bottom();
    }
    throw std::runtime_error("Not standard location");
}

Rotation Piece::getRotation() const {
	return _rotation;
}

size_t Piece::hash() const {
    return ((static_cast<size_t>(_edges[0]) << 0) | (static_cast<size_t>(_edges[1]) << 8) |
            (static_cast<size_t>(_edges[2]) << 16) | (static_cast<size_t>(_edges[3]) << 24));
}

void Piece::print(std::ostream& output) const {
	output << _id;
	if (Base != _rotation) {
		output << " [" << 90 * static_cast<int>(_rotation) << "]";
	}
}

////////////////////////////////////////////
void throwParseException(int id, const std::string& line) {
    std::ostringstream stream;
    stream << "Puzzle ID " << id << " has wrong data : " << line;
    throw std::runtime_error(stream.str());
}