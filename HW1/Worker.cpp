#include "Worker.h"

Worker::Worker(std::atomic_bool& some_worker_solved)
        : some_worker_solved(some_worker_solved),
          current_solver_index(0),
          solved(false),
          solution_lookup(nullptr) {
}

void Worker::addSolver(Solver&& solver) {
    solvers.emplace_back(std::move(solver));
}

std::future<std::unique_ptr<LookupBase>> Worker::trySolveAsync() {
    return std::async(std::launch::async, [this]() { return trySolve(); });
}

std::unique_ptr<LookupBase> Worker::trySolve() {
    if (solvers.empty()) {
        return nullptr;
    }
    while (nextStep()) {}
    return solved ? std::move(solution_lookup) : nullptr;
}

// Return true iff should continue iterating
bool Worker::nextStep() {
    if (some_worker_solved) {
        return false;
    }

    auto& solver = solvers[current_solver_index];
    bool should_continue = solver.nextStep();
    if (!should_continue) {
        if (solver.hasSolved()) {
            solved = true;
            solution_lookup = solver.takeLookup();
            // Stop other workers now, instead of waiting for them to finish unnecessarily.
            // (when std::future is destroyed, it is implicitly waited on)
            some_worker_solved = true;
            return false;
        } else {
            solvers.erase(solvers.begin() + current_solver_index);
            if (solvers.empty()) {
                return false;
            }
            current_solver_index = current_solver_index % solvers.size();
        }
    } else {
        current_solver_index = (current_solver_index + 1) % solvers.size();
    }

    return true;
}
