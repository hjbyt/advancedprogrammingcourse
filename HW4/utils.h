#pragma once

template<int A, int B>
struct power {
    static const int value = A * power<A, B - 1>::value;
};

template<int A>
struct power<A, 0> {
    static const int value = 1;
};

// Note: this inefficient implementation of cartesian product is an adaptation from a simple example in python
//       (https://docs.python.org/2/library/itertools.html#itertools.product)
//       in the real world we would have used a better implementation from a library (e.g. Boost).
template <typename T>
std::vector<std::vector<T>> cartesian_product(const std::vector<std::vector<T>>& vectors) {
    std::vector<std::vector<T>> result;
    result.emplace_back(std::vector<T>());
    for (auto& vector : vectors) {
        std::vector<std::vector<T>> new_result;
        for (std::vector<T>& partial_vec : result) {
            for (T value : vector) {
                std::vector<T> new_vec(partial_vec);
                new_vec.emplace_back(value);
                new_result.emplace_back(new_vec);
            }
        }
        result = new_result;
    }
    return result;
}
