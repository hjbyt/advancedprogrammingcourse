#include "DirectPuzzleValidator.h"

#include <algorithm>

DirectPuzzleValidator::DirectPuzzleValidator(Puzzle& puzzle) : 
	PuzzleValidatorBase(puzzle)
{
}

bool DirectPuzzleValidator::checkStraightEdgesAndSetPossibleDimensions() {
	std::unordered_map<DirectionInt, int> straight_edges;
	for (auto edge : DIRECTIONS) {
		straight_edges[edge] = 0;
	}
	for (auto& piece : _puzzle.pieces) {
		for (auto location : DIRECTIONS) {
			auto is_straight = (EdgeType::Straight == piece.getEdge(location));
			straight_edges[location] += static_cast<int>(is_straight);
		}
	}
	auto possible_dimensions = _puzzle.getPossibleDimensions();
	std::vector<Dimensions> new_possible_dimensions;
	std::copy_if(possible_dimensions.begin(), possible_dimensions.end(), std::back_inserter(new_possible_dimensions),
		[this, &straight_edges](Dimensions dimensions) -> bool {
		return this->isPossibleDimensions(dimensions, straight_edges);
	});
	if (new_possible_dimensions.empty()) {
		return false;
	}
	_puzzle.setPossibleDimensions(new_possible_dimensions);
	return true;
}

bool DirectPuzzleValidator::hasCornerPiece(CornerType cornerType) {
	Direction location1, location2;
	switch (cornerType) {
	case CornerType::TopLeft:
		location1 = Direction::Top;
		location2 = Direction::Left;
		break;
	case CornerType::TopRight:
		location1 = Direction::Top;
		location2 = Direction::Right;
		break;
	case CornerType::BottomLeft:
		location1 = Direction::Bottom;
		location2 = Direction::Left;
		break;
	case CornerType::BottomRight:
		location1 = Direction::Bottom;
		location2 = Direction::Right;
		break;
	default:
		// Added the default case to satisify compiler's warning about potentially uninitialized parameters
		location1 = Direction::Top;
		location2 = Direction::Top;
		assert(false);
	}
	for (auto& piece : _puzzle.pieces) {
		if ((EdgeType::Straight == piece.getEdge(location1)) && (EdgeType::Straight == piece.getEdge(location2))) {
			return true;
		}
	}
	return false;
}

bool DirectPuzzleValidator::isEdgesSumZero() {
	int edge_sum_horizontal = 0;
	int edge_sum_vertical = 0;
	for (auto& piece : _puzzle.pieces) {
		edge_sum_horizontal += piece.getEdge(Direction::Left);
		edge_sum_horizontal += piece.getEdge(Direction::Right);
		edge_sum_vertical += piece.getEdge(Direction::Top);
		edge_sum_vertical += piece.getEdge(Direction::Bottom);
	}
	return edge_sum_horizontal == 0 && edge_sum_vertical == 0;
}

bool DirectPuzzleValidator::isPossibleDimensions(Dimensions dimensions, std::unordered_map<CornerTypeInt, int>& straight_count) {
	auto tester = [](int side_length, int first_count, int second_count) -> bool {
		if (side_length > first_count || side_length > second_count) { return false; }
		if (first_count != second_count) { return false; }
		return true;
	};
	return (tester(dimensions.width, straight_count[Direction::Top], straight_count[Direction::Bottom]) &&
		tester(dimensions.height, straight_count[Direction::Left], straight_count[Direction::Right]));
}