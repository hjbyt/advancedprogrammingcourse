#pragma once

#include "Lookup.h"

// Note: this is a simple wrapper that makes sure the data is allocated on the heap
template<int D, int K>
class LookupWrapper {
public:
    template<class InputIterator>
    LookupWrapper(InputIterator start, InputIterator last);
    std::vector<const PuzzlePiece<D, K>*> get(const PuzzlePiece<D, K>& piece_template) const;

private:
    std::unique_ptr<Lookup<D, K>> lookup;
};

template<int D, int K>
template<class InputIterator>
LookupWrapper<D, K>::LookupWrapper(InputIterator start, InputIterator last)
        : lookup(std::make_unique<Lookup<D, K>>(start, last)) {
}

template<int D, int K>
std::vector<const PuzzlePiece<D, K>*> LookupWrapper<D, K>::get(const PuzzlePiece<D, K>& piece_template) const {
    return lookup->get(piece_template);
}

template<class InputIterator>
LookupWrapper<InputIterator::value_type::DIMENSTIONS, InputIterator::value_type::SIZE_RANGE>
groupPuzzlePieces(InputIterator start, InputIterator end) {
    return LookupWrapper<InputIterator::value_type::DIMENSTIONS, InputIterator::value_type::SIZE_RANGE>(start, end);
}
