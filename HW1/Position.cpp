#include "Position.h"
#include <assert.h>

Position::Position() {
	for (auto location : EDGE_LOCATIONS) {
		requirements[(int)location] = EdgeType::Invalid;
	}
}

Position::Position(const Position& base, const Piece& piece) {
	for (auto location : EDGE_LOCATIONS) {
		this->requirements[(int)location] = base.requirements[(int)location];
	}
	this->piece = piece;
}

void Position::addRequirements(EdgeLocation location, EdgeType type) {
	assert(requirements[(int)location] == EdgeType::Invalid);
	requirements[(int)location] = type;
}	

EdgeType Position::getRequirements(EdgeLocation location) {
	return requirements[(int)location];
}

void Position::clearRequirements(EdgeLocation location) {
	requirements[(int)location] = EdgeType::Invalid;
}

void Position::setPiece(const Piece& new_piece) {
	assert(!used());
	piece = new_piece;
}

void Position::clearPiece() {
	assert(used());
	piece = Piece();
}

bool Position::used() {
	return (-1 != piece.id());
}