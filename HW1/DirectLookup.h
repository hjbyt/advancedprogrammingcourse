#pragma once

#include "LookupBase.h"

class DirectLookup : public LookupBase {
public:
	DirectLookup(int width, int height, const std::vector<Piece>& pieces);
	virtual ~DirectLookup() { }

protected:
	virtual void addPieceLookups(const Piece& piece) override;
	virtual void removePieceLookups(const Piece& piece) override;
	virtual Piece fromPossiblePieces(LookupEdgeType top, LookupEdgeType bottom, LookupEdgeType left, LookupEdgeType right) override;

	std::vector<int> piece_ids[NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS][NUMBER_OF_EDGES_LOCATIONS];
};