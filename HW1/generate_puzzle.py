import argparse
from itertools import chain
import random

DEFAULT_STRAIGHT_PERCENTAGE = 1.0/3.0

def main():
    args = parse_args()
    if not (0 <= args.straight <= 1):
        print("Error: 'straight' argument must be in range [0, 1].")
        return
    generate_puzzle(args.width, args.height, args.straight)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--width', type=int, default=5)
    parser.add_argument('--height', type=int, default=5)
    parser.add_argument('--straight', type=float, default=DEFAULT_STRAIGHT_PERCENTAGE,
                        help="percentage of inner edges which will be straight")
    return parser.parse_args()


def generate_puzzle(width, height, straight=DEFAULT_STRAIGHT_PERCENTAGE):
    puzzle = Puzzle(width, height, straight)
    puzzle.randomize()
    puzzle.print()


class Puzzle:
    def __init__(self, width, height, straight=DEFAULT_STRAIGHT_PERCENTAGE):
        self.width = width
        self.height = height
        self.straight = straight
        self.n = width * height
        self.pieces = [Piece() for _ in range(self.n)]

    def __getitem__(self, key):
        x, y = key
        return self.pieces[y * self.width + x]

    def __setitem__(self, key, value):
        x, y = key
        self.pieces[y * self.width + x] = value

    def iter_lines(self):
        for y in range(self.height):
            yield (self[x, y] for x in range(self.width))

    def randomize(self):
        # Set ids
        ids = list(range(1, self.width * self.height + 1))
        random.shuffle(ids)
        for id, piece in zip(ids, self.pieces):
            piece.id = id

        non_straight = (1 - self.straight) / 2
        weights = [non_straight, self.straight, non_straight]
        internal_edges_count = (self.width-1) * self.height + (self.height - 1) * self.width
        random_edges = random.choices([-1, 0, 1], weights=weights, k=internal_edges_count)
        random_edges_iter = iter(random_edges)

        # Randomize inner edges
        for x in range(self.width):
            for y in range(self.height):
                if x < self.width-1:
                    edge = next(random_edges_iter)
                    self[x, y].edges[RIGHT] = edge
                    self[x + 1, y].edges[LEFT] = -edge
                if y < self.height - 1:
                    edge = next(random_edges_iter)
                    self[x, y].edges[BOTTOM] = edge
                    self[x, y + 1].edges[TOP] = -edge

    def print(self):
        print(f'NumElements = {self.n}')
        pieces = self.pieces[:]
        random.shuffle(pieces)
        for piece in pieces:
            edges = ' '.join("{:>2}".format(e) for e in piece.edges)
            print('{:<2}  {}'.format(piece.id, edges))

        print()
        print('Possible solution:')
        for line in self.iter_lines():
            ids = (piece.id for piece in line)
            print(' '.join("{:>2}".format(i) for i in ids))


LEFT = 0
TOP = 1
RIGHT = 2
BOTTOM = 3


class Piece:
    def __init__(self, edges=None):
        self.edges = [0, 0, 0, 0] if edges is None else edges
        self.id = None

    def __repr__(self):
        return f'Piece({self.id}){self.edges}'


if __name__ == '__main__':
    main()
