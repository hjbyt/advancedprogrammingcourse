#include "RotationPuzzleValidator.h"

#include <algorithm>

RotationPuzzleValidator::RotationPuzzleValidator(Puzzle& puzzle) :
	PuzzleValidatorBase(puzzle)
{
}

bool RotationPuzzleValidator::isEdgesSumZero() {
	int edge_sum = 0;
	for (auto& piece : _puzzle.pieces) {
		for (auto direction : DIRECTIONS) {
			edge_sum += piece.getEdge(direction);
		}
	}
	return edge_sum == 0;
}

bool RotationPuzzleValidator::checkStraightEdgesAndSetPossibleDimensions() {
	int straight_edges_count = 0;
	for (auto& piece : _puzzle.pieces) {
		for (auto location : DIRECTIONS) {
			auto is_straight = (EdgeType::Straight == piece.getEdge(location));
			straight_edges_count += static_cast<int>(is_straight);
		}
	}

	auto possible_dimensions = _puzzle.getPossibleDimensions();
	std::vector<Dimensions> new_possible_dimensions;
	std::copy_if(possible_dimensions.begin(), possible_dimensions.end(), std::back_inserter(new_possible_dimensions),
		[straight_edges_count](Dimensions dimensions) -> bool {
			int needed_straights = dimensions.width * 2 + dimensions.height * 2;
		// Since when we are allowed to rotate switching between the dimensions would produce an isomorphic result
		// we only need to check a single case
		return dimensions.width <= dimensions.height && needed_straights <= straight_edges_count;
	});
	_puzzle.setPossibleDimensions(new_possible_dimensions);
	return new_possible_dimensions.size() >= 1;
}

bool RotationPuzzleValidator::hasCornerPiece(CornerType /*cornerType*/) {
	for (auto& piece : _puzzle.pieces) {
		if (isCorner(piece)) {
			if (used_corners.end() == find(used_corners.begin(), used_corners.end(), piece.id())) {
				used_corners.push_back(piece.id());
				return true;
			}
		}
	}
	return false;
}

bool RotationPuzzleValidator::isCorner(const Piece& piece) {
	bool top_or_bottom = (EdgeType::Straight == piece.top()) || (EdgeType::Straight == piece.bottom());
	bool left_or_right = (EdgeType::Straight == piece.left()) || (EdgeType::Straight == piece.right());
	return (top_or_bottom && left_or_right);
}