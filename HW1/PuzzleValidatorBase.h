#pragma once

#include <iostream>
#include <array>

#include "Puzzle.h"

class PuzzleValidatorBase {
public:
	PuzzleValidatorBase(Puzzle& puzzle);
	virtual ~PuzzleValidatorBase() {}

	bool validateCanSolve(std::ostream& output);

protected:
	enum CornerType {
		TopLeft,
		TopRight,
		BottomLeft,
		BottomRight,
	};
	// Note: this is required since gcc complains that CornerType is not hashable
	using CornerTypeInt = int;
	static const std::array<CornerType, 4> CORNER_TYPES;
	static std::string cornerTypeToString(CornerType corner_type);


	virtual bool checkStraightEdgesAndSetPossibleDimensions() = 0;
	virtual bool hasCornerPiece(CornerType cornerType) = 0;
	virtual bool isEdgesSumZero() = 0;
	Puzzle& _puzzle;
};