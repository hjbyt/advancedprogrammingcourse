#pragma once

#include <array>

enum class EdgeLocation {
	Left = 0,
	Top,
	Right,
	Bottom
};

const std::array<EdgeLocation, 4> EDGE_LOCATIONS = { EdgeLocation::Left, EdgeLocation::Top, EdgeLocation::Right, EdgeLocation::Bottom };