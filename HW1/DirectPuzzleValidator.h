#pragma once

#include "PuzzleValidatorBase.h"

class DirectPuzzleValidator : public PuzzleValidatorBase {
public:
	DirectPuzzleValidator(Puzzle& puzzle);
	virtual ~DirectPuzzleValidator() {}

protected:
	virtual bool checkStraightEdgesAndSetPossibleDimensions();
	virtual bool hasCornerPiece(CornerType cornerType);
	virtual bool isEdgesSumZero();

private:
	bool isPossibleDimensions(Dimensions dimensions, std::unordered_map<CornerTypeInt, int>& straight_count);
};