#pragma once

#include "Direction.h"
#include "Edges.h"
#include "Piece.h"

struct Position {
	EdgeType requirements[EDGE_LOCATIONS.size()];
	Piece piece;

    Position();
	Position(const Position& base, const Piece& piece);
	void addRequirements(EdgeLocation location, EdgeType type);
	EdgeType getRequirements(EdgeLocation location);
	void clearRequirements(EdgeLocation location);
	void setPiece(const Piece& new_piece);
	void clearPiece();
	bool used();
};
