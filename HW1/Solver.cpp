#include "Solver.h"

Solver::Slot::Slot(const Location&& location, const std::vector<Piece>&& candidates, int current_candidate_index)
        : location(location),
          candidates(candidates),
          current_candidate_index(current_candidate_index) {
}


Solver::Solver(std::unique_ptr<LookupBase> lookup)
        : lookup(std::move(lookup)),
          total_slots(this->lookup->width() * this->lookup->height()),
          empty_slots(total_slots),
          next_step_type(NextSlot),
          solved(false) {
    slots.reserve(total_slots);
}

std::unique_ptr<LookupBase> Solver::takeLookup() {
    return std::move(lookup);
}

bool Solver::hasSolved() {
    return solved;
}

// Return true iff should continue iterating
bool Solver::nextStep() {
    switch (next_step_type) {
        case NextSlot: {
            if (hasNextSlot()) {
                nextSlot();
                next_step_type = NextCandidate;
            } else {
                // Finished successfully
                solved = true;
                return false;
            }
            break;
        }
        case NextCandidate: {
            if (hasNextCandidate()) {
                nextCandidate();
                lookup->setPosition(slot().location, candidate());
                next_step_type = NextSlot;
            } else {
                // Backtrack
                slots.pop_back();
                // If we backtracked all the way to the beginning - there is no solution.
                if (slots.empty()) {
                    return false;
                }
                lookup->clear(slot().location);
                empty_slots += 1;
                next_step_type = NextCandidate;
            }
            break;
        }
        default:
            assert(false);
    }

    return true;
}

bool Solver::hasNextSlot() {
    return empty_slots > 0;
}

void Solver::nextSlot() {
    assert(hasNextSlot());
    auto location = lookup->getNextLocation();
    auto candidates = lookup->getOptionsForLocation(location);
    Slot next(std::move(location), std::move(candidates), -1);
    empty_slots -= 1;
    slots.emplace_back(next);
}

Solver::Slot& Solver::slot() {
    return slots.back();
}

bool Solver::hasNextCandidate() {
    return slot().current_candidate_index + 1 < (int)slot().candidates.size();
}

void Solver::nextCandidate() {
    assert(hasNextCandidate());
    slot().current_candidate_index += 1;
}

const Piece& Solver::candidate() {
    return slot().candidates[slot().current_candidate_index];
}
