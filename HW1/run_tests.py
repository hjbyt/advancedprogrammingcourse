import os
import argparse
import subprocess
import time

#TODO: we should validate solutions here,
#      and/or make sure the the exe is built with assertions enabled.
#      (the makefile build without asserts, thus the solution isn't validated in the produced exe)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--exe', type=str, default=r'./ex3')
    parser.add_argument('--dir', type=str, default='Tests')
    return parser.parse_args()

def run_test(args, name):
    input_name = os.path.join(args.dir, name, 'in.txt')
    out_name = os.path.join(args.dir, name, 'out.txt')
    reference_out = os.path.join(args.dir, name, 'reference_out.txt')
    second_reference = os.path.join(args.dir, name, 'reference_out-2.txt')
    error_name = os.path.join(args.dir, name, 'error.txt')
    rotate_file_marker = os.path.join(args.dir, name, 'rotate')
    if not os.path.isfile(input_name):
        print("Skipping test {} since the input is missing".format(input_name))
        return
    if os.path.isfile(out_name):
        os.remove(out_name)
    if os.path.isfile(error_name):
        expected_error = open(error_name, 'r').read()
        print("Expecting a failure")
    else:
        expected_error = None
        print("Expecting a solution")
    try:
        exe_args = [args.exe, input_name, out_name]
        if os.path.isfile(rotate_file_marker):
            exe_args += ['-rotate']
        output = subprocess.check_output(' '.join(exe_args), stderr=subprocess.STDOUT, shell=True)
    except subprocess.CalledProcessError as e:
        assert expected_error, 'The program failed while we expected it to work, msg: {}, return code {}'.format(e.output, e.returncode)
    else:
        # Note - We don't validate the output because changing the algorithm might change the result
        #        and there might be a number of correct results
        assert os.path.isfile(out_name), "Expected the output file to exist"
        real_result = open(out_name, 'r').read()
        if os.path.isfile(reference_out):
            reference_result = open(reference_out, 'r').read()
            if os.path.isfile(second_reference):
                second_ref_result = open(second_reference, 'r').read()
                assert reference_result == real_result or second_ref_result == real_result, 'Output mismatch on reference output, expected "{}" or "{}", real "{}"'.format(reference_result, second_ref_result, real_result)
            else:
                assert reference_result == real_result, 'Output mismatch on reference output, expected "{}", real "{}"'.format(reference_result, real_result)
        elif expected_error:
            # We normalize for windows/linux incompatabilities and because output is bytes and not str
            expected_error = expected_error.replace('\r\n', '\n')
            real_result = "".join(map(chr, real_result)).replace('\r\n', '\n')
            assert expected_error == real_result, 'Error mismatch, expected "{}", found "{}"'.format(expected_error, real_result)

def main():
    args = parse_args()
    for test in os.listdir(args.dir):
        print('------- Running test {}'.format(test))
        beginning = time.time()
        run_test(args, test)
        end = time.time()
        duration = end - beginning
        print('Finishes test {} in {} seconds'.format(test, duration))


if __name__ == '__main__':
    main()