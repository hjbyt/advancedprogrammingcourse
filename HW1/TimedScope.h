#pragma once

#include <chrono>

struct TimedScope {
    TimedScope();
    ~TimedScope();
private:
    using clock = std::chrono::steady_clock;
    clock::time_point beginning;
};
