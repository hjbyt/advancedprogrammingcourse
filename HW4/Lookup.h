#pragma once

#include <limits>
#include <vector>
#include <memory>
#include <cmath>

#include "PuzzlePiece.h"
#include "utils.h"

template<int D, int K>
class Lookup {
public:
    template<class InputIterator>
    Lookup(InputIterator start, InputIterator last);
    std::vector<const PuzzlePiece<D, K>*> get(const PuzzlePiece<D, K>& piece_template) const;

private:
    enum {
        SIDE_OPTIONS = 2 * K + 1
    };
    static int toLookup(int option);
    static std::vector<int> getOptions(int edge_type);
    template<typename Sides>
    size_t toIndex(const Sides& sides) const;
    template<typename Sides>
    std::vector<const PuzzlePiece<D, K>*>& toPieceSet(const Sides& sides);
    template<typename Sides>
    const std::vector<const PuzzlePiece<D, K>*>& toPieceSet(const Sides& sides) const;
    void addPiece(const PuzzlePiece<D, K>& piece);

    //Note: this data structure provides very fast lookup, but might be space inefficient in case of large SIDE_OPTIONS.
    //      if the data is sparse (i.e. number of different pieces << SIDE_OPTIONS ^ SIDES), then it might be better to use
    //      hash tables (std::unordered_map) instead of arrays. They provide O(1) lookup on average, but in practice
    //      are much slower then direct array lookup.
    //      Also note that it would be better to hold a count of pieces of each kind, instead of holding a pointer to the
    //      original pieces - but this seems to go against the expected API (which dereferences the returned pieces).
    std::vector<const PuzzlePiece<D, K>*> pieces[power<SIDE_OPTIONS, PuzzlePiece<D, K>::SIDES>::value];
};

template<int D, int K>
template<class InputIterator>
Lookup<D, K>::Lookup(InputIterator start, InputIterator last) {
    while (start != last) {
        addPiece(*start);
        start++;
    }
}

template<int D, int K>
int Lookup<D, K>::toLookup(int option) {
    return option + K;
}

//Note: in a nice language (such as Python, D, C#) we would return a generator instead of a vector,
//      but doing it in vanilla c++ (no Boost) is too tedious.
template<int D, int K>
std::vector<int> Lookup<D, K>::getOptions(int edge_type) {
    std::vector<int> vec;
    if (std::numeric_limits<int>::min() == edge_type) {
        for (int i = -K; i <= K; i++) {
            vec.emplace_back(i);
        }
    } else {
        vec.emplace_back(edge_type);
    }
    return vec;
}

//Note: in a nice language (such as Python, D, C#) we would return a generator instead of a vector,
//      but doing it in vanilla c++ (no Boost) is too tedious.
template<int D, int K>
std::vector<const PuzzlePiece<D, K>*> Lookup<D, K>::get(const PuzzlePiece<D, K>& piece_template) const {
    std::vector<const PuzzlePiece<D, K>*> results;

    std::vector<std::vector<int>> side_options;
    std::transform(piece_template.sides.begin(), piece_template.sides.end(), std::back_inserter(side_options),
                   [](int side) -> std::vector<int> { return getOptions(side); });

    for (std::vector<int>& sides : cartesian_product(side_options)) {
        for (auto piece : toPieceSet(sides)) {
            results.emplace_back(piece);
        }
    }

    return results;
}

// See https://eli.thegreenplace.net/2015/memory-layout-of-multi-dimensional-arrays for indexing explanation
template<int D, int K>
template<typename Sides>
size_t Lookup<D, K>::toIndex(const Sides& sides) const {
    size_t index = 0;
    size_t n = sides.size();
    for (size_t i = 0; i < n; ++i) {
        auto side = sides[i];
        auto lookup = toLookup(side);
        index += lookup * pow(SIDE_OPTIONS, n - i - 1);
    }
    return index;
}

template<int D, int K>
template<typename Sides>
std::vector<const PuzzlePiece<D, K>*>& Lookup<D, K>::toPieceSet(const Sides& sides) {
    auto index = toIndex(sides);
    return this->pieces[index];
}

template<int D, int K>
template<typename Sides>
const std::vector<const PuzzlePiece<D, K>*>& Lookup<D, K>::toPieceSet(const Sides& sides) const {
    auto index = toIndex(sides);
    return this->pieces[index];
}

template<int D, int K>
void Lookup<D, K>::addPiece(const PuzzlePiece<D, K>& piece) {
    toPieceSet(piece.sides).emplace_back(&piece);
}
