#include "TimedScope.h"

#include <iostream>

TimedScope::TimedScope() : beginning(clock::now()) {}

TimedScope::~TimedScope() {
    clock::time_point end = clock::now();
    using seconds_double = std::chrono::duration<double, std::ratio<1, 1>>;
    auto duration_in_seconds = seconds_double(end - beginning).count();
    std::cout << "Finished in: " << duration_in_seconds << " Seconds" << std::endl;
}
