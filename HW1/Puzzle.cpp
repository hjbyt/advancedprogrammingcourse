#include "Puzzle.h"
#include <iostream>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <numeric>
#include <ctime>
#include <thread>
#include <future>

#include "DirectLookup.h"
#include "RotationLookup.h"
#include "Worker.h"
#include "TimedScope.h"

//#define PRINT_TIME

////////////////////////////////////////////
Puzzle::Puzzle(std::vector<Piece>& pieces)
        : pieces(pieces),
          solution_lookup(nullptr) {
}

std::vector<Dimensions> Puzzle::getPossibleDimensions() {
    if (!possible_dimensions.empty()) {
        return possible_dimensions;
    }
    int pieces_count = static_cast<int>(pieces.size());
    int root = static_cast<int>(sqrt(pieces_count));
    for (int side = 1; side <= root; side++) {
        if (0 == (pieces_count % side)) {
            int other_side = pieces_count / side;
            possible_dimensions.push_back(Dimensions(side, other_side));
            if (other_side != side) {
                possible_dimensions.push_back(Dimensions(other_side, side));
            }
        }
    }
    return possible_dimensions;
}

void Puzzle::setPossibleDimensions(std::vector<Dimensions>& new_possible_dimensions) {
    possible_dimensions = new_possible_dimensions;
}

bool Puzzle::solve(bool rotations_allowed, unsigned int thread_count) {
    doSolve(rotations_allowed, thread_count);
    if (this->solution_lookup != nullptr) {
        assert(isSolutionValid());
        return true;
    }
    return false;
}

void Puzzle::doSolve(bool rotations_allowed, unsigned int thread_count) {
    /*
     * This method is a bit of a mouthful, so we'll explain what it does:
     * The method runs in parallel (actually, over a thread pool) all the attempts to solve the puzzle
     * in all the different dimensions (since each dimension is completely independent from other dimensions).
     * This means that if there is a solution we'll find it a lot faster since it's solution won't have to block
     * waiting for wrong dimensions to fail. Once a single solution is found this function terminates all the other
     * futures and that way we wait the minimum time possible.
     */
#ifdef PRINT_TIME
    TimedScope timed_scope;
#endif

    // This is used to stop all threads once one has successfully solved
    std::atomic_bool some_worker_solved(false);

    // Make workers
    std::vector<std::unique_ptr<Worker>> workers;
    unsigned worker_count = thread_count;
    workers.reserve(worker_count);
    for (unsigned int i = 0; i < worker_count; ++i) {
        workers.emplace_back(std::make_unique<Worker>(Worker(some_worker_solved)));
    }

    // Add solvers to workers
    unsigned int current_worker_index = 0;
    for (auto dimensions : possible_dimensions) {
        std::unique_ptr<LookupBase> lookup;
        if (rotations_allowed) {
            lookup = std::make_unique<RotationLookup>(dimensions.width, dimensions.height, pieces);
        } else {
            lookup = std::make_unique<DirectLookup>(dimensions.width, dimensions.height, pieces);
        }
        workers[current_worker_index]->addSolver(Solver(std::move(lookup)));
        current_worker_index = (current_worker_index + 1) % workers.size();
    }

    // Take one worker out to be the worker of this thread
    std::unique_ptr<Worker> main_worker = std::move(workers.back());
    workers.pop_back();

    // Start other workers in separate threads
    std::vector<std::future<std::unique_ptr<LookupBase>>> futures;
    futures.reserve(workers.size());
    for (auto& worker : workers) {
        futures.emplace_back(worker->trySolveAsync());
    }

    // Start main worker here
    this->solution_lookup = main_worker->trySolve();

    // Return now if main solver was successful
    if (this->solution_lookup != nullptr) {
        return;
    }

    // Wait for and check results of the other workers
    for (auto& future : futures) {
        this->solution_lookup = future.get();
        if (this->solution_lookup != nullptr) {
            return;
        }
    }
}

void Puzzle::print(std::ostream& output) const {
    int height = solution_lookup->height();
    int width = solution_lookup->width();
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            auto piece = solution_lookup->getAt(Location(x, y));
            piece.print(output);
            if (x < width - 1) {
                output << " ";
            }
        }
        output << std::endl;
    }
}

bool Puzzle::isSolutionValid() {
    std::vector<bool> ids_seen(pieces.size() + 2, false);
    for (int y = 0; y < solution_lookup->height(); ++y) {
        for (int x = 0; x < solution_lookup->width(); ++x) {
            Piece piece = solution_lookup->getAt(Location(x, y));
            if (ids_seen[piece.id()] == true) {
                return false;
            } else {
                ids_seen[piece.id()] = true;
            }
            if (x == solution_lookup->width() - 1) {
                if (piece.right() != EdgeType::Straight) return false;
            } else {
                if (piece.right() != -(solution_lookup->getAt(Location(x + 1, y)).left())) return false;
            }
            if (y == solution_lookup->height() - 1) {
                if (piece.bottom() != EdgeType::Straight) return false;
            } else {
                if (piece.bottom() != -(solution_lookup->getAt(Location(x, y + 1)).top())) return false;
            }
            if (x == 0) {
                if (piece.left() != EdgeType::Straight) return false;
            } else {
                if (piece.left() != -(solution_lookup->getAt(Location(x - 1, y)).right())) return false;
            }
            if (y == 0) {
                if (piece.top() != EdgeType::Straight) return false;
            } else {
                if (piece.top() != -(solution_lookup->getAt(Location(x, y - 1)).bottom())) return false;
            }
        }
    }
    for (unsigned int id = 1; id <= pieces.size(); ++id) {
        if (!ids_seen[id]) return false;
    }
    return true;
}
