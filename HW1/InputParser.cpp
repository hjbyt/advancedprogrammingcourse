#include "InputParser.h"

#include <memory>
#include <regex>
#include <unordered_map>

#include "DirectPuzzleValidator.h"
#include "RotationPuzzleValidator.h"

const unsigned int DEFAULT_THREAD_COUNT = 4;

InputParser::InputParser(int argc, char* argv[]) : 
    rotations_allowed(false),
    thread_count(DEFAULT_THREAD_COUNT),
    use_cout(false),
    use_cin(false) {

    bool got_input = false;
    bool got_output = false;
    bool got_rotate = false;
    bool got_thread_count = false;

    // Parse arguments
    valid_parameters = PrintUsage;
    for (int i = 1; i < argc; ++i) {
        std::string arg = std::string(argv[i]);
        if (arg == "-threads") {
            if (got_thread_count || i == argc - 1) {
                return;
            }
            i += 1;
            arg = std::string(argv[i]);
            try {
                thread_count = std::stoi(arg);
            } catch (std::invalid_argument&) {
                return;
            }
            got_thread_count = true;
        } else if (arg == "-rotate") {
            if (got_rotate) {
                return;
            }
            rotations_allowed = true;
            got_rotate = true;
        } else {
            if (!got_input) {
                input_file = arg;
                got_input = true;
            } else if (!got_output) {
                output_file = arg;
                got_output = true;
            } else {
                return;
            }
        }
    }
    if (!got_input || !got_output) {
        return;
    }

    // Open files
    valid_parameters = InvalidInput;
    if (output_file == "-") {
        use_cout = true;
    } else {
        try {
            output_file_stream.open(output_file);
            output_file_stream.exceptions(std::istream::failbit | std::istream::badbit);
        } catch (std::exception& e) {
            std::cout << "Can't open output file: " << e.what() << std::endl;
            return;
        }
    }
    if (input_file == "-") {
        use_cin = true;
    } else {
        try {
            input_file_stream.open(input_file);
            input_file_stream.exceptions(std::istream::failbit | std::istream::badbit);
        } catch (std::exception& e) {
            getOutput() << "Can't open input file: " << e.what() << std::endl;
            return;
        }
    }

    // Finished successfully
    valid_parameters = ValidInput;
}

std::unique_ptr<PuzzleValidatorBase> InputParser::getValidator(Puzzle& puzzle) {
    if (rotations_allowed) {
        return std::make_unique<RotationPuzzleValidator>(puzzle);
    }
    else {
        return std::make_unique<DirectPuzzleValidator>(puzzle);
    }
}

bool InputParser::parsePieces(std::vector<Piece>& pieces) {
    int pieces_count = 0;
    try {
        parseFirstLine(pieces_count);
    }
    catch (std::istream::failure&) {
        throw std::runtime_error("Error reading input");
    }
    std::vector<std::string> parse_exceptions;
    pieces = tryParsePieceLines(pieces_count, parse_exceptions);

    std::sort(pieces.begin(), pieces.end(), [](const Piece& a, const Piece& b) -> bool {
        return a.id() > b.id();
    });

    bool are_missing = checkForMissingElements(pieces, pieces_count);
    bool are_wrong_elemets = checkForWrongElements(pieces, pieces_count);
    bool are_parse_exception = printParseExceptions(parse_exceptions);
    if (are_missing || are_wrong_elemets || are_parse_exception) {
        return false;
    }
    return true;
}

std::ostream& InputParser::getOutput() {
    if (use_cout) {
        return std::cout;
    }
    return output_file_stream;
}
std::istream& InputParser::getInput() {
    if (use_cin) {
        return std::cin;
    }
    return input_file_stream;
}

void InputParser::parseFirstLine(int& pieces_count) {
    std::string line;
    std::getline(getInput(), line);
    std::regex pattern("NumElements *= *(\\d*)\\r?");
    std::smatch match;
    if (!std::regex_match(line, match, pattern)) {
        throw std::runtime_error("Incorrect format for first line");
    }

    try {
        pieces_count = std::stoi(match[1]);
    }
    catch (std::invalid_argument&) {
        throw std::runtime_error("Expected a number");
    }
    catch (std::out_of_range&) {
        throw std::runtime_error("Number out of range");
    }
}

std::vector<Piece> InputParser::tryParsePieceLines(int piece_count, std::vector<std::string>& parse_exceptions) {
    std::vector<Piece> pieces;
    pieces.reserve(piece_count);
    for (int i = 0; i < piece_count; ++i) {
        try {
            std::string line;
            std::getline(getInput(), line);
            Piece piece = Piece::fromLine(line);
            pieces.push_back(piece);
        }
        catch (std::ios_base::failure&) {
            // This is a file-reading error, like if the file is too short
            // In this case we don't show any special errors, but the rest of the logic will show
            // the user all the correct errors
            return pieces;
        }
        catch (std::runtime_error& ex) {
            parse_exceptions.push_back(ex.what());
        }
    }

    return pieces;
}

bool InputParser::checkForMissingElements(const std::vector<Piece>& pieces, int expected_count) {
    bool are_missing = false;
    std::vector<int> missing_ids;
    std::unordered_map<int, bool> was_found;
    for (int id = 1; id <= expected_count; id++) {
        was_found[id] = false;
    }
    for (auto& piece : pieces) {
        was_found[piece.id()] = true;
    }
    for (auto pair : was_found) {
        if (!pair.second) {
            are_missing = true;
            missing_ids.push_back(pair.first);
        }
    }
    if (are_missing) {
        getOutput() << "Missing puzzle element(s) with the following IDs: " << prepareForPrinting(missing_ids) << std::endl;
    }
    return are_missing;
}

bool InputParser::checkForWrongElements(const std::vector<Piece>& pieces, int expected_count) {
    bool have_errors = false;
    std::vector<int> invalid_ids;
    for (auto& piece : pieces) {
        if ((piece.id() > expected_count) || (piece.id() <= 0)) {
            invalid_ids.push_back(piece.id());
            have_errors = true;
        }
    }
    if (have_errors) {
        getOutput() << "Puzzle of size " << expected_count << " cannot have the following IDs: " << prepareForPrinting(invalid_ids)
               << std::endl;
    }
    return have_errors;
}

bool InputParser::printParseExceptions(const std::vector<std::string>& parse_exceptions) {
    bool exceptions_present = false;
    for (auto& message : parse_exceptions) {
        getOutput() << message << std::endl;
        exceptions_present = true;
    }
    return exceptions_present;
}

std::string InputParser::prepareForPrinting(std::vector<int>& id_list) {
    std::string seperator = ", ";
    std::stringstream stream;
    std::sort(id_list.begin(), id_list.end());
    std::copy(id_list.begin(), id_list.end(), std::ostream_iterator<int>(stream, seperator.c_str()));
    std::string s = stream.str();
    s.erase(s.end() - seperator.length(), s.end());
    return s;
}