#pragma once

#include <atomic>
#include <memory>
#include "LookupBase.h"

class Solver {
    struct Slot {
        const Location location;
        //TODO: instead of candidates+current_candidate_index, keep only candidates_iter
        const std::vector<Piece> candidates;
        int current_candidate_index;

        Slot(const Location&& location, const std::vector<Piece>&& candidates, int current_candidate_index);
    };

    enum NextStepType {
        NextSlot,
        NextCandidate
    };

    std::unique_ptr<LookupBase> lookup;
    int total_slots;
    int empty_slots;
    std::vector<Slot> slots;
    NextStepType next_step_type;
    bool solved;

public:
    Solver(std::unique_ptr<LookupBase> lookup);
    std::unique_ptr<LookupBase> takeLookup();
    bool nextStep();
    bool hasSolved();

private:
    bool hasNextSlot();
    void nextSlot();
    Slot& slot();
    bool hasNextCandidate();
    void nextCandidate();
    const Piece& candidate();
};