#pragma once

#include <vector>
#include <memory>
#include <future>
#include "LookupBase.h"
#include "Solver.h"

class Worker {
    std::vector<Solver> solvers;
    std::atomic_bool& some_worker_solved;
    unsigned int current_solver_index;
    bool solved;
    std::unique_ptr<LookupBase> solution_lookup;

public:
    Worker(std::atomic_bool& some_worker_solved);
    void addSolver(Solver&& solver);
    std::future<std::unique_ptr<LookupBase>> trySolveAsync();
    std::unique_ptr<LookupBase> trySolve();

private:
    bool nextStep();
};
