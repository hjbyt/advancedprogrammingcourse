#include "DirectLookup.h"

#include <algorithm>

DirectLookup::DirectLookup(int width, int height, const std::vector<Piece>& pieces) :
	LookupBase(width, height)
{
	for (const auto& piece : pieces) {
		addPieceLookups(piece);
	}
}

void DirectLookup::addPieceLookups(const Piece& piece) {
	loopPieceLocations(piece, 1, possible_pieces);
	auto t = toLookup(piece.top());
	auto b = toLookup(piece.bottom());
	auto l = toLookup(piece.left());
	auto r = toLookup(piece.right());
	piece_ids[(int)t][(int)b][(int)l][(int)r].push_back(piece.id());
	if (0 != unique_pieces[(int)t][(int)b][(int)l][(int)r]) {
		return;
	}
	loopPieceLocations(piece, 1, unique_pieces);
}

void DirectLookup::removePieceLookups(const Piece& piece) {
	loopPieceLocations(piece, -1, possible_pieces);
	auto t = toLookup(piece.top());
	auto b = toLookup(piece.bottom());
	auto l = toLookup(piece.left());
	auto r = toLookup(piece.right());
	auto& id_vector = piece_ids[(int)t][(int)b][(int)l][(int)r];
	id_vector.erase(std::remove(id_vector.begin(), id_vector.end(), piece.id()), id_vector.end());
	if (0 != possible_pieces[(int)t][(int)b][(int)l][(int)r]) {
		return;
	}
	loopPieceLocations(piece, -1, unique_pieces);
}

Piece DirectLookup::fromPossiblePieces(LookupEdgeType top, LookupEdgeType bottom, LookupEdgeType left, LookupEdgeType right) {
	return Piece(
			piece_ids[(int)top][(int)bottom][(int)left][(int)right][0], 
			{ toEdgeType(left), toEdgeType(top), toEdgeType(right), toEdgeType(bottom) }, 
			Rotation::Base);
}