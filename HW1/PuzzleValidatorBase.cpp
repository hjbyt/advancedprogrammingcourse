#include "PuzzleValidatorBase.h"


PuzzleValidatorBase::PuzzleValidatorBase(Puzzle& puzzle) : _puzzle(puzzle) {}

const std::array<PuzzleValidatorBase::CornerType, 4> PuzzleValidatorBase::CORNER_TYPES = { CornerType::TopLeft, CornerType::TopRight, CornerType::BottomLeft, CornerType::BottomRight };

bool PuzzleValidatorBase::validateCanSolve(std::ostream& output) {
	bool should_try_solve = true;
	if (!checkStraightEdgesAndSetPossibleDimensions()) {
		output << "Cannot solve puzzle: wrong number of straight edges" << std::endl;
		should_try_solve = false;
	}
	for (CornerType cornerType : CORNER_TYPES) {
		if (!hasCornerPiece(cornerType)) {
			output << "Cannot solve puzzle: missing corner element: " << cornerTypeToString(cornerType) << std::endl;
			should_try_solve = false;
		}
	}
	if (!isEdgesSumZero()) {
		output << "Cannot solve puzzle: sum of edges is not zero" << std::endl;
		should_try_solve = false;
	}
	return should_try_solve;
}

std::string PuzzleValidatorBase::cornerTypeToString(CornerType corner_type) {
	switch (corner_type) {
	case CornerType::TopLeft:
		return "TL";
	case CornerType::TopRight:
		return "TR";
	case CornerType::BottomLeft:
		return "BL";
	case CornerType::BottomRight:
		return "BR";
	}
	throw std::runtime_error("Not standard corner type");
}