#include "ex4headers.h"

void main1() {
    std::list<Puzzle2dPiece<5>> pieces = {{0, 3,  2, -5},
                                          {0, -2, 2, -5}};
    auto groups = groupPuzzlePieces(pieces.begin(), pieces.end());
    auto some_pieces = groups.get({0, std::numeric_limits<int>::min(), 2, -5});
    for (auto piece_ptr : some_pieces) {
        std::cout << *piece_ptr << std::endl;
    }
}

void main2() {
    std::list<Puzzle3dPiece<1>> pieces = {{0, 1,  1, 1, -1, -1},
                                          {0, -1, 1, 1, 1,  1}};
    auto groups = groupPuzzlePieces(pieces.begin(), pieces.end());
    auto some_pieces = groups.get({0, 1, 1, 1, 1, 1});
    for (auto piece_ptr : some_pieces) {
        std::cout << *piece_ptr << std::endl;
    }
}

void main3() {
    auto joker = std::numeric_limits<int>::min();
    std::list<Puzzle3dPiece<1>> pieces = {{0, 1,  1, 1, -1, -1},
                                          {0, -1, 1, 1, 1,  1}};
    auto groups = groupPuzzlePieces(pieces.begin(), pieces.end());
    auto some_pieces = groups.get({0, joker, 1, 1, joker, joker});
    for (auto piece_ptr : some_pieces) {
        std::cout << *piece_ptr << std::endl;
    }
}

void main4() {
    std::vector<Puzzle2dPiece<5>> pieces = {{0, 3,  2, -5},
                                            {0, -2, 2, -5}};
    auto groups = groupPuzzlePieces(pieces.begin(), pieces.end());
    auto some_pieces = groups.get({0, 3, 2, std::numeric_limits<int>::min()});
    for (auto piece_ptr : some_pieces) {
        std::cout << *piece_ptr << std::endl;
    }
}

#define J std::numeric_limits<int>::min()

void main_generic1() {
    std::vector<PuzzlePiece<1, 1>> pieces = {
            {0, 1},
            {0, -1}
    };
    auto pattern = {J, 1};
    auto groups = groupPuzzlePieces(pieces.begin(), pieces.end());
    auto some_pieces = groups.get(pattern);
    for (auto piece_ptr : some_pieces) {
        std::cout << *piece_ptr << std::endl;
    }
}

void main_generic2() {
    std::vector<PuzzlePiece<2, 1>> pieces = {
            {0, 1,  1, 0},
            {0, -1, 1, 0}
    };
    auto pattern = {J, -1, 1, J};
    auto groups = groupPuzzlePieces(pieces.begin(), pieces.end());
    auto some_pieces = groups.get(pattern);
    for (auto piece_ptr : some_pieces) {
        std::cout << *piece_ptr << std::endl;
    }
}

void main_generic3() {
    std::vector<PuzzlePiece<3, 1>> pieces = {
            {0, 1,  1, 0, 1,  -1},
            {0, -1, 1, 0, -1, -1}
    };
    auto pattern = {J, -1, 1, J, -1, -1};
    auto groups = groupPuzzlePieces(pieces.begin(), pieces.end());
    auto some_pieces = groups.get(pattern);
    for (auto piece_ptr : some_pieces) {
        std::cout << *piece_ptr << std::endl;
    }
}

void main_generic4() {
    std::vector<PuzzlePiece<4, 1>> pieces = {
            {0, 1,  1, 0, 1,  -1, 0, 0},
            {0, -1, 1, 0, -1, -1, 1, 0}
    };
    auto pattern = {J, -1, 1, J, -1, -1, 1, 0};
    auto groups = groupPuzzlePieces(pieces.begin(), pieces.end());
    auto some_pieces = groups.get(pattern);
    for (auto piece_ptr : some_pieces) {
        std::cout << *piece_ptr << std::endl;
    }
}

void main_generic5() {
    std::vector<PuzzlePiece<5, 2>> pieces = {
            {-2, -1, 0, 1, 2, -2, -1, 0, 1, 2},
            {-2, -1, 0, 1, 2, -2, -1, 1, 1, 2},
            {0,  2,  0, 1, 2, -2, -1, 1, 1, 2},
            {0,  -1, 0, 0, 0, 0,  -1, 0, 0, 2},
            {-2, -1, 0, 1, 2, -2, -1, 0, 1, 2},
    };
    auto pattern = {J, -1, 0, 1, J, -2, -1, J, J, J};
    auto groups = groupPuzzlePieces(pieces.begin(), pieces.end());
    auto some_pieces = groups.get(pattern);
    for (auto piece_ptr : some_pieces) {
        std::cout << *piece_ptr << std::endl;
    }
}

int main() {
    try {
        std::cout << "case 1:" << std::endl;
        main1();
        std::cout << "case 2:" << std::endl;
        main2();
        std::cout << "case 3:" << std::endl;
        main3();
        std::cout << "case 4:" << std::endl;
        main4();

        std::cout << "case generic 1:" << std::endl;
        main_generic1();
        std::cout << "case generic 2:" << std::endl;
        main_generic2();
        std::cout << "case generic 3:" << std::endl;
        main_generic3();
        std::cout << "case generic 4:" << std::endl;
        main_generic4();
        std::cout << "case generic 5:" << std::endl;
        main_generic5();
    } catch (std::exception& ex) {
        std::cout << "Unexpected error: " << ex.what() << std::endl;
        return 1;
    }
    return 0;
}
