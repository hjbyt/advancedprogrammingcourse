#pragma once

#include <vector>
#include <stdexcept>

template<typename T>
class Vector2d {
public:
    Vector2d(int width, int height);

    Vector2d(int width, int height, T value);

    T& access(int x, int y);

    const T get(int x, int y) const;

    void set(int x, int y, T value);

    int width;
    int height;
    std::vector<T> elements;
};

template<typename T>
Vector2d<T>::Vector2d(int width, int height)
        : width(width),
          height(height),
          elements(width * height) {}

template<typename T>
Vector2d<T>::Vector2d(int width, int height, T value)
        : width(width),
          height(height),
          elements(width * height, value) {}

template<typename T>
T& Vector2d<T>::access(int x, int y) {
    return elements[y * width + x];
}

template<typename T>
const T Vector2d<T>::get(int x, int y) const {
    return elements[y * width + x];
}

template<typename T>
void Vector2d<T>::set(int x, int y, T value) {
    access(x, y) = value;
}