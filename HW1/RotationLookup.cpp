#include "RotationLookup.h"

#include <algorithm>

RotationLookup::RotationLookup(int width, int height, const std::vector<Piece>& pieces) :
	LookupBase(width, height)
{
	for (const auto& piece : pieces) {
		addPieceLookups(piece);
	}
}

void RotationLookup::addPieceLookups(const Piece& piece) {
	for (auto rotation : ROTATIONS) {
		Piece current = Piece(piece, rotation);
		loopPieceLocations(current, 1, possible_pieces);
		auto t = toLookup(current.top());
		auto b = toLookup(current.bottom());
		auto l = toLookup(current.left());
		auto r = toLookup(current.right());
		piece_ids[(int)t][(int)b][(int)l][(int)r].push_back(IdAndRotation(current.id(), current.getRotation()));
		if (0 == unique_pieces[(int)t][(int)b][(int)l][(int)r]) {
			loopPieceLocations(current, 1, unique_pieces);
		}
	}
}

void RotationLookup::removePieceLookups(const Piece& piece) {
	for (auto rotation : ROTATIONS) {
		Piece current = Piece(piece, rotation);
		loopPieceLocations(current, -1, possible_pieces);
		auto t = toLookup(current.top());
		auto b = toLookup(current.bottom());
		auto l = toLookup(current.left());
		auto r = toLookup(current.right());
		auto& id_vector = piece_ids[(int)t][(int)b][(int)l][(int)r];
		id_vector.erase(std::remove(id_vector.begin(), id_vector.end(), IdAndRotation(current.id(), current.getRotation())), id_vector.end());
		if (0 == possible_pieces[(int)t][(int)b][(int)l][(int)r]) {
			loopPieceLocations(current, -1, unique_pieces);
		}
	}
}

Piece RotationLookup::fromPossiblePieces(LookupEdgeType top, LookupEdgeType bottom, LookupEdgeType left, LookupEdgeType right) {
	IdAndRotation id_and_rotations = piece_ids[(int)top][(int)bottom][(int)left][(int)right][0];

	for (int i = 0; i < static_cast<int>(id_and_rotations.rotation); i++) {
		rotatePiecesLeft(left, top, right, bottom);
	}

	return Piece(
		id_and_rotations.id,
		{ toEdgeType(left), toEdgeType(top), toEdgeType(right), toEdgeType(bottom) },
		id_and_rotations.rotation);
}

RotationLookup::IdAndRotation::IdAndRotation(const int id, const Rotation rotation) {
	this->id = id;
	this->rotation = rotation;
}

bool RotationLookup::IdAndRotation::operator==(const IdAndRotation& other) {
	return ((this->id == other.id) && (this->rotation == other.rotation));
}

void RotationLookup::rotatePiecesLeft(LookupEdgeType& left, LookupEdgeType& top, LookupEdgeType& right, LookupEdgeType& bottom) {
	LookupEdgeType original_top = top;
	top = right;
	right = bottom;
	bottom = left;
	left = original_top;
}