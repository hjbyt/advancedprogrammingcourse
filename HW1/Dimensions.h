#pragma once

struct Dimensions {
	int width;
	int height;

	Dimensions(int width, int height);
};